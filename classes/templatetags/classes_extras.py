from django import template
from classes.models import Expert
from django.http import Http404
register = template.Library()


@register.simple_tag
def get_user_labels(pk):
    u = Expert.objects.get(id=pk)
    return u.get_labels()


@register.simple_tag
def get_user_label_count(pk):

    u = Expert.objects.get(id=pk)
    return len(u.get_labels())


@register.simple_tag
def get_highscore():
    experts = Expert.objects.all()
    highscore = 0
    for expert in experts:
        cnt = len(expert.get_labels())
        if cnt > highscore:
            highscore = cnt
    return highscore

@register.simple_tag
def get_last_label_id(pk):
    expert = Expert.objects.get(id=pk)
    labels = expert.label_set.all().order_by('-date')
    if len(labels) > 0:
        return labels[0].id
    else:
        return 0
    
@register.simple_tag
def get_ranking():
    experts = Expert.objects.all()
    sorted_experts = sorted(experts, key=lambda e: len(e.get_labels()), reverse=True)
    return sorted_experts

@register.simple_tag
def get_rank(pk):
    experts = get_ranking()
    rank = 1
    for expert in experts:
        if expert.pk == pk:
            break
        rank += 1
    return rank

@register.simple_tag
def get_diff_to_next(pk):
    ranking = get_ranking()
    if len(ranking) < 2:
        return "-"
    rank = get_rank(pk)
    if rank == 1:
        a = ranking[rank-1].pk
        b = ranking[rank].pk
    else:
        a = ranking[rank-2].pk
        b = ranking[rank-1].pk
        
    a = get_user_label_count(a)
    b = get_user_label_count(b)
    return a-b

@register.simple_tag
def get_total_label_count():
    experts = Expert.objects.all()
    count = 0
    for expert in experts:
        count += len(expert.get_labels())
    return count
