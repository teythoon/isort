from django.urls import path, include
from django.conf.urls import url, include
from . import views

urlpatterns = [
    path('', views.classification, name='index'),
    path('examples/', views.show_examples, name='examples'),
    path('images/', views.image_list, name='image-list'),
    path('imagefolders/', views.image_folders, name='image-folder-list'),
    path('loadfolder/<str:folder>', views.load_images_from_folder, name='load-folder'),
    path('mark_human/<int:pk>', views.mark_human, name='mark-human'),
    path('mark_human/', views.mark_human, name='mark-human'),
    path('mark_non_human/<int:pk>', views.mark_non_human, name='mark-non-human'),
    path('mark_non_human/', views.mark_non_human, name='mark-non-human'),
    path('accounts/profile/', views.login_redirect, name='login-redirect'),
    path('accounts/profile/<int:pk>', views.ExpertDetailView.as_view(), name='expert-detail'),
    path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('delete_label/<int:pk>', views.LabelDeleteView.as_view(), name='delete-label'),
    path('ranking', views.ranking, name='ranking'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]
