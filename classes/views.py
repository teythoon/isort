from django.http import HttpResponse, Http404

from django.urls import reverse_lazy

from django.shortcuts import render
from django.shortcuts import redirect

from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.views import View

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles.templatetags.staticfiles import static

from classes.models import Image
from classes.models import Label
from classes.models import Expert
import classes.models

from . import forms

from fs import open_fs
import random


from django.contrib.auth.decorators import login_required


def image_list(request):
    images = Image.objects.all()
    labels = Label.objects.all()
    context = {'object_list': images,
               'label_count': len(labels)}
    return render(request, 'classes/image_list.html', context)


@login_required
def image_folders(request):
    path = static('images/')
    path = staticfiles_storage.path('images/')
    print(path)
    img_root = open_fs(path)
    folders = img_root.listdir('/')
    context = {'folder_list': folders}
    return render(request, 'classes/image_folder_list.html', context)


@login_required
def load_images_from_folder(request, folder):
    path = staticfiles_storage.path('images/'+folder)
    img_folder = open_fs(path)
    files = sorted(img_folder.walk.files(filter=['*.png']))
    nr_created = 0
    for f in files:
        img, created = Image.objects.get_or_create(file_name=folder+f)
        if created:
            img.save()
            nr_created += 1

    context = {'folder': folder,
               'file_list': files,
               'nr_of_images': len(files),
               'nr_created': nr_created}
    return render(request, 'classes/image_folder_content.html', context)


def show_examples(request):
    subpath = 'examples/human'
    path = staticfiles_storage.path(subpath)
    img_folder = open_fs(path)
    files_human = sorted(img_folder.walk.files(filter=['*.png']))
    files_human = ["/static/"+subpath+s for s in files_human]

    subpath = 'examples/non_human'
    path = staticfiles_storage.path(subpath)
    img_folder = open_fs(path)
    files_non_human = sorted(img_folder.walk.files(filter=['*.png']))
    files_non_human = ["/static/"+subpath+s for s in files_non_human]

    subpath = 'examples/cutouts'
    path = staticfiles_storage.path(subpath)
    img_folder = open_fs(path)
    cutouts = sorted(img_folder.walk.files(filter=['*.png']))
    cutouts = ["/static/"+subpath+s for s in cutouts]

    
    context = {'humans': files_human,
               'non_humans': files_non_human,
               'cutouts': cutouts}

    return render(request, 'classes/examples.html', context)


def classification(request):
    #img_file = classes.models.get_unlabeled_image()
    img_file = classes.models.get_random_image()
    path = staticfiles_storage.url("images/"+str(img_file))
    context = {'image': img_file, 'file_path': path}
    return render(request, 'classes/classify.html', context)


@login_required
def mark_human(request, pk=None):
    if pk is not None:
        img = Image.objects.get(id=pk)
        label = Label(name="Human",
                      image=img,
                      expert=request.user)
        label.save()
    return redirect('index')


@login_required
def mark_non_human(request, pk=None):
    if pk is not None:
        img = Image.objects.get(id=pk)
        label = Label(name="Non-Human",
                      image=img,
                      expert=request.user)
        label.save()
    return redirect('index')


class ExpertDetailView(LoginRequiredMixin, UpdateView):
    login_url = 'accounts/login/'
    redirect_field_name = 'next'
    model = Expert
    template_name = 'classes/expert_detail.html'
    form_class = forms.ExpertForm

    def get_object(self, *args, **kwargs):
        obj = super(ExpertDetailView, self).get_object(*args, **kwargs)
        if not obj.pk == self.request.user.pk:
            raise Http404
        return obj

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        return super().form_valid(form)


@login_required
def login_redirect(request):
    return redirect('expert-detail', pk=request.user.pk)

class LabelDeleteView(LoginRequiredMixin, DeleteView):
    login_url = 'accounts/login/'
    model = Label
    redirect_field_name = 'next'
    success_url = '/'
    
    def get_object(self, *args, **kwargs):
        obj = super(LabelDeleteView, self).get_object(*args, **kwargs)
        if not obj.expert.pk  == self.request.user.pk:
            print
            raise Http404
        return obj

    
def ranking(request):
    context = {}
    return render(request, 'classes/ranking.html', context)
